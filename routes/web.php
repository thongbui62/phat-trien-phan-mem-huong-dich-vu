<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
})->name('homepage');

Route::get('login.html','LoginController@getLogin')->name('login');
Route::post('login.html','LoginController@setLogin')->name('setLogin');

Route::get('register.html','LoginController@getRegister')->name('register');
Route::post('register.html','LoginController@setRegister')->name('setRegister');
Route::get('logout.html','LoginController@logout')->name('logout');

Route::group(['prefix'=>'admin','middleware'=>'checklogin'],function () {
    Route::get('index.html','AdminController@getIndex')->middleware('checkadmin')->name('admin/index');

    Route::get('get-all-user.html','AdminController@getAllUser')->name('getAllUser');

    Route::get('add-user.html','AdminController@addUser')->name('addUser');

    Route::post('add-user.html','AdminController@setAddUser')->name('setAddUser');

    Route::get('delete-user/{id?}','AdminController@deleteUser')->name('deleteUser');

    Route::get('edit-user/{id?}','AdminController@geteditController')->name('getEditUser');

    Route::get('add-document.html','AdminController@addDocument')->name('add-document');

    Route::post('add-document.html','AdminController@setAddDocument')->name('setAddDocument');

    Route::get('document.html','AdminController@getAllDocument')->name('document');

    Route::get('change-active','AdminController@setCheckActive')->name('active');


    Route::get('get-all-category.html','AdminController@getAllCategory')->name('getAllCategory');

    Route::get('add-category.html','AdminController@getAddCategory')->name('addCategory');

    Route::post('add-category.html','AdminController@setAddCategory')->name('setAddCategory');



    Route::get('show-chart.html',function(){
        return view('admin.char');
    })->name('showChart');
    Route::get('mailbox.html',function(){
        return view('admin.mailbox');
    })->name('mailbox');



});

    Route::get('index.html','PageController@index')->name('users/index');

    Route::get('detail.html','PageController@detail')->name('detail');

    Route::get('intro.html','PageController@intro')->name('users/intro');

    Route::get('gop-y.html','PageController@gop_y')->name('users/gop-y');

    Route::get('lien-he.html','PageController@contact')->name('users/lien-he');

    Route::get('view-detail.html','PageController@view_detail')->name('users/view-detail');

Route::get('auth/facebook', 'FacebookAuthController@redirectToProvider')->name('facebook.login') ;
Route::get('auth/facebook/callback', 'FacebookAuthController@handleProviderCallback');
Route::get('ajax/{id}','ajaxController@get_detail');
Route::get('ajax2/{id}','ajaxController@edit');
Route::get('add-tran/{id}','ajaxController@add_tran');
Route::get('ajax21','ajaxController@editheader');
Route::get('ajax1','ajaxController@view_detail');
Route::post('edit-tran/{id}','PageController@edit_tran')->name('edit_tran');
Route::get('del-tran/{id}','PageController@del_tran')->name('del_tran');
Route::get('ajax-add','ajaxController@ajax_add');
Route::post('add-tran/{id}','PageController@add_tran')->name('add-tran');
Route::get('ajaxdata/{id}','ajaxController@ajaxdata');
Route::get('ajaxdata1','ajaxController@ajaxdata1');
Route::get('ajaxdata2/{e}','ajaxController@ajaxdata2');
Route::get('del-succ','ajaxController@del_succ');





