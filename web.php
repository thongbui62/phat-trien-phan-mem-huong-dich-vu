<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
})->name('homepage');

Route::get('login.html','LoginController@getLogin')->name('login');
Route::post('login.html','LoginController@setLogin')->name('setLogin');

Route::get('register.html','LoginController@getRegister')->name('register');
Route::post('register.html','LoginController@setRegister')->name('setRegister');
Route::get('logout.html','LoginController@logout')->name('logout');

Route::group(['prefix'=>'admin','middleware'=>'checklogin'],function () {
    Route::get('index.html',function(){
        return view('admin.index');
    })->middleware('checkadmin')->name('admin/index');

    Route::get('get-all-user.html',function(){
        return view('admin.user');
    })->name('getAllUser');

    Route::get('get-all-category.html',function(){
        return view('admin.category');
    })->name('getAllCategory');

    Route::get('add-category.html',function(){
        return view('admin.addCategory');
    })->name('addCategory');

    Route::get('add-user.html',function(){
        return view('admin.addUser');
    })->name('addUser');

    Route::get('show-chart.html',function(){
        return view('admin.char');
    })->name('showChart');
    Route::get('mailbox.html',function(){
        return view('admin.mailbox');
    })->name('mailbox');

    Route::get('document.html',function(){
        return view('admin.document');
    })->name('document');

    Route::get('add-document.html',function () {
        return view('admin.addDocument');
    })->name('add-document');
});

Route::group(['prefix'=>'users','middleware'=>'checklogin'],function(){
    Route::get('index.html',function(){
        return view('users.index');
    })->name('users/index');

    Route::get('detail.html',function(){
        return view('users.detail');
    })->name('detail');

    Route::get('intro.html',function () {
        return view('users.intro');
    })->name('users/intro');

    Route::get('gop-y.html',function(){
        return view('users.gopy');
    })->name('users/gop-y');

    Route::get('lien-he.html',function(){
        return view('users.lienhe');
    })->name('users/lien-he');

    Route::get('view-detail.html',function () {
        return view('users.viewDetail');
    })->name('users/view-detail');
});


Route::get('auth/facebook', 'FacebookAuthController@redirectToProvider')->name('facebook.login') ;
Route::get('auth/facebook/callback', 'FacebookAuthController@handleProviderCallback');

