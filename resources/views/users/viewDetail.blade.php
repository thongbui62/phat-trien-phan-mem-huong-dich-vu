@extends('users.master')
@section('user')	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="text-center" style="font-weight: bold;color: blue">Danh sách chi tiêu </h4>
				<table class="table table-hover">
					<h3 class="alert alert-info"><em>Tháng 9</em> <a href=""><span style="float: right;font-size: 15px" class="fa fa-long-arrow-right">Tháng 8</span></a></h3>
					<thead>
						<tr>
							<th>STT</th>
							<th>TÊN CHI TIÊU</th>
							<th>SỐ TIỀN</th>
							<th>GHI CHÚ</th>
							<th>Thời gian</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</th>
							<td>ĐÓNG HỌC PHÍ</td>
							<td>10.000.000 VNĐ</td>
							<td>Đã nộp</td>
							<td>26/09/2018</td>
						</tr>
						<tr>
							<td>2</th>
							<td>ĐÓNG HỌC PHÍ</td>
							<td>10.000.000 VNĐ</td>
							<td>Đã nộp</td>
							<td>26/09/2018</td>
						</tr>
						<tr>
							<td>3</th>
							<td>ĐÓNG HỌC PHÍ</td>
							<td>10.000.000 VNĐ</td>
							<td>Đã nộp</td>
							<td>26/09/2018</td>
						</tr>
						<tr>
							<td>4</th>
							<td>ĐÓNG HỌC PHÍ</td>
							<td>10.000.000 VNĐ</td>
							<td>Đã nộp</td>
							<td>26/09/2018</td>
						</tr>
						<tr>
							<td>5</th>
							<td>ĐÓNG HỌC PHÍ</td>
							<td>10.000.000 VNĐ</td>
							<td>Đã nộp</td>
							<td>26/09/2018</td>
						</tr>
					</tbody>
				</table>
				<nav aria-label="Page navigation">
				  <ul class="pagination">
				    <li>
				      <a href="#" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				      </a>
				    </li>
				    <li><a href="#">1</a></li>
				    <li><a href="#">2</a></li>
				    <li><a href="#">3</a></li>
				    <li><a href="#">4</a></li>
				    <li><a href="#">5</a></li>
				    <li>
				      <a href="#" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li>
				  </ul>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10"></div>
			<div class="col-sm-2">
				<a href="" class="btn btn-danger">Export to excel</a>
			</div>
		</div>
	</div>
@endsection