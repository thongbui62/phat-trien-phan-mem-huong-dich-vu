@extends('users.master');
@section('user')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2 class="text-center">THÔNG TIN LIÊN HỆ</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.472214803562!2d105.73227131432473!3d21.053793885984877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313455af9072ccf9%3A0xadb5f7555c46683d!2zxJDhuqFpIEjhu41jIEPDtG5nIE5naGnhu4dwIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1537868828809" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>

		</p>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<h4 class="text-center">Gửi nội dung góp ý</h4>
			<form>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Họ tên</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vui lòng nhập họ tên vào ô trống">
			   
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Số điện thoại</label>
			    <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Vui lòng nhập số điện thoại vào ô trống">
			  </div>
			 
			  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Nội dung</label>
				    <textarea name="gop-y" class="form-control ckeditor" id="ckeditor" rows="3"></textarea>
			  </div>
			  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Đánh giá của người dùng</label>
				    <p>
				    	<i class="fa fa-star"></i>
				    	<i class="fa fa-star"></i>
				    	<i class="fa fa-star"></i>
				    	<i class="fa fa-star"></i>
				    	<i class="fa fa-star"></i>
				    </p>
			  </div>
			  <div class="form-group">
			  	<div class="g-recaptcha" data-sitekey="6LfRAHIUAAAAAD6Eubf5DwZzw8VLsZCGI8MLIptb"></div>
			  </div>
			  <button type="submit" class="btn btn-primary">Gửi góp ý</button>
			</form>
		</div>

		<div class="col-sm-6 text-center">
			<h4 class="text-center">Thông tin liên hệ</h4>
			<h5>TRƯỜNG ĐẠI HỌC CÔNG NGHIỆP HÀ NỘI</h5>
			<p>Số 298 đường Cầu Diễn, quận Bắc Từ Liêm, Hà Nội</p>
			<p>+84 243 765 5121</p>
			<p>Email :dhcnhn@haui.edu.vn </p>

		</div>
	</div>
</div>
	
@endsection