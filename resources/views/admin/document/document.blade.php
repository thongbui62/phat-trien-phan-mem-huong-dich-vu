@extends('admin.master')
@section('content')
    <?php
            function createTrDocument($stt,$id,$name,$content,$file,$created_at){
                $xhtml = '<tr>
                  <td>'.$stt.'</td>
                  <td>'.$id.'</td>
                  <td><a href="#">'.$name.'</a></td>
                  <td>'.$content.'</td>
                  <td>'.$file.'</td>
                  <td>'.$created_at.'</td>
                </tr>';

                return $xhtml;
            }
    ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 ">
				<h2 class="text-center">TÀI LIỆU LƯU TRỮ HỆ THỐNG</h2>
				<div class="box-body ">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>ID</th>
                  <th>TÊN TÀI LIỆU</th>
                  <th>NỘI DUNG</th>
                  <th>FILE ĐÍNH KÈM</th>
                  <th>NGÀY ĐĂNG</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $stt = 0;
                    foreach($data as $v){
                        $stt++;
                        $id = $v['id'];
                        $name = $v['name'];
                        $content = substr($v['content'],0,50);
                        $file = $v['file'];
                        $created_at = $v['created_at'];
                        echo createTrDocument($stt,$id,$name,$content,$file,$created_at);
                    }



                ?>
                </tbody>
              </table>
              <div class="pagi text-center">
                 <nav aria-label="Page navigation">
                  <ul class="pagination">
                      <li>
                          <a href="#" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                          </a>
                      </li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li>
                          <a href="#" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                          </a>
                      </li>
                  </ul>
              </nav>
              </div>
             
            </div>
			</div>
		</div>
	</div>
@endsection