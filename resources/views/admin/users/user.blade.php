@extends('admin.master')
@section('content')
<?php
  function createRow($stt,$id,$username,$email,$level,$active,$linkDelete,$linkEdit){
    $active_id = $active;
    $level = ($level == 1) ? 'Admin' : 'Member';
    $active = ($active == 1) ? 'checked' : '';
    $xhtml = '<tr>
                  <td>'.$stt.'</td>
                  <td>'.$username.'</td>
                  <td>'.$email.'</td>
                  <td> '.$level.'</td>
                  <td><input type="checkbox" '.$active.' id="'.$id.'" onclick="showdata('.$active_id.','.$id.')"></td>
                  <td>
                      <a href="'.$linkEdit.'"><i class=" btn btn-success fa fa-edit"></i></a>
                      <a href="'.$linkDelete.'" onclick="return deleteUser();"><i class="btn btn-danger fa fa-remove"></i></a>
                  </td>
                </tr>';
    return $xhtml;
  }
 ?>

 <script>
   function deleteUser(){
    let d =  window.confirm('Bạn chắc chắn muốn xóa người dùng này?');
    if(d == true){
      return true;
    }else{
      return false;
    }
     
   }
 </script>
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header cla">
              <h3 class="box-title">DANH SÁCH USERS</h3>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>USERNAME</th>
                  <th>EMAIL</th>
                  <th>LEVEL</th>
                  <th>ACTIVE</th>
                  <th>ACTIONS</th>
                </tr>
                </thead>
                <tbody>
                
                <?php
                  $stt = 0;
                  foreach($list_user as $k=>$v){
                    $stt++;
                    $linkDelete = route('deleteUser').'/'.$v->id;
                    $linkEdit = route('getEditUser').'/'.$v->id;
                    echo createRow($stt,$v->id,$v->name,$v->email,$v->level,$v->active,$linkDelete,$linkEdit);
                  }
                 ?>
                
                </tfoot>
              </table>
              <div class="pagi text-center">
                {{$list_user->links()}}
              </div>
             
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <script>
        function showdata(active,id){
          let check;
          if(active == false){
              check = 1;
          }else{
              check = 0;
          }

        $.ajax({
              url : '<?php route("active") ?>',
              type : 'GET',
              data : {check : check,id : id},
              success : function(data){
                          console.log(data);
                      }
        });
      }
      </script>
@endsection