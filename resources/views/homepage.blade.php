<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quản lý chi tiêu cá nhân</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{!!asset('homepage/img/favicon.png')!!}" rel="icon">
  <link href="{!!asset('homepage/img/apple-touch-icon.png')!!}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{!!asset('homepage/lib/bootstrap/css/bootstrap.min.css')!!}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{!!asset('homepage/lib/animate/animate.min.css')!!}" rel="stylesheet">
  <link href="{!!asset('homepage/lib/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet">
  <link href="{!!asset('homepage/lib/ionicons/css/ionicons.min.css')!!}" rel="stylesheet">
  <link href="{!!asset('homepage/lib/magnific-popup/magnific-popup.css')!!}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{!!asset('homepage/css/style.css')!!}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Avilon
    Theme URL: https://bootstrapmade.com/avilon-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#intro" class="scrollto">SAVING MONEY</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
         
          <li><a href="{!! route('register') !!}">Register</a></li>
         
          <li><a href="{!! route('login') !!}">Login</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

   
    <div class="product-screens">

      <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
        <img src="{!!asset('homepage/img/product-screen-1.png')!!}" alt="">
      </div>

      <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
        <img src="{!!asset('homepage/img/product-screen-2.png')!!}" alt="">
      </div>

      <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
        <img src="{!!asset('homepage/img/product-screen-3.png')!!}" alt="">
      </div>

    </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">Giới thiệu phần mềm</h3>
          <span class="section-divider"></span>
          <p class="section-description">
            Saving money là phần mềm hỗ trợ quản lý chi tiêu cá nhân trên nền tảng website
           <br>
            Chúng tôi cam kết đem lại sản phẩm tốt nhất
          </p>
        </div>

        <div class="row">
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="{!! asset('homepage/img/about-img.jpg')!!}" alt="">
          </div>

          <div class="col-lg-6 content wow fadeInRight">
            <h2>Tại sao cần quản lý chi tiêu ?</h2>
            <h3>Quản lý chi tiêu cá nhân là cách tốt nhất để kiểm soát được dòng tiền chúng ta đã chi tiêu</h3>
            <p>
              Quản lý chi tiêu đưa ra cho chúng ta những giải pháp để chi tiêu phù hợp,góp phần vào sự thành công của chúng ta trong tương lai
            </p>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i>Đưa ra giải pháp chi tiêu hợp lý</li>
              <li><i class="ion-android-checkmark-circle"></i>Góp phần vào sự giàu có của bản thân</li>
              <li><i class="ion-android-checkmark-circle"></i>Làm chủ được dòng tiền đang có</li>
            </ul>

            
          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Product Featuress Section
    ============================-->
    <section id="features">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 offset-lg-4">
            <div class="section-header wow fadeIn" data-wow-duration="1s">
              <h3 class="section-title">Chức năng chính</h3>
              <span class="section-divider"></span>
            </div>
          </div>

          <div class="col-lg-4 col-md-5 features-img">
            <img src="{!!asset('homepage/img/product-features.png')!!}" alt="" class="wow fadeInLeft">
          </div>

          <div class="col-lg-8 col-md-7 ">

            <div class="row">

              <div class="col-lg-6 col-md-6 box wow fadeInRight">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                <h4 class="title"><a href="">Thống kê chi tiêu theo ngày</a></h4>
                <p class="description">Phần mềm cho phép thống kê chi tiêu theo từng ngày</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.1s">
                <div class="icon"><i class="ion-ios-flask-outline"></i></div>
                <h4 class="title"><a href="">Quản lý chi tiêu theo từng tháng</a></h4>
                <p class="description">Phần mềm cho phép thống kê chi tiêu theo từng tháng</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.2s">
                <div class="icon"><i class="ion-social-buffer-outline"></i></div>
                <h4 class="title"><a href="">Gợi ý chi tiêu</a></h4>
                <p class="description">Phần mềm hỗ trợ trong việc gợi ý chi tiêu phù hợp với thu nhập của bạn</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.3s">
                <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                <h4 class="title"><a href="">Đọc thông tin chi tiêu</a></h4>
                <p class="description">Phần mềm cung cấp các bài báo,video thông minh trong việc quản lý chi tiêu cá nhân</p>
              </div>
            </div>

          </div>

        </div>

      </div>

    </section><!-- #features -->

    <!--==========================
      Product Advanced Featuress Section
    ============================-->
   

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Hãy hành động</h3>
            <p class="cta-text"> Đăng ký tài khoản và quản lý chi tiêu cá nhân một cách phù hợp nhất</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Go to register</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      More Features Section
    ============================-->
  

    <!--==========================
      Clients
    ============================-->
    <section id="clients">
      <div class="container">

        <div class="row wow fadeInUp">

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-1.png')!!}" alt="">
          </div>

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-2.png')!!}" alt="">
          </div>

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-3.png')!!}" alt="">
          </div>

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-4.png')!!}" alt="">
          </div>

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-5.png')!!}" alt="">
          </div>

          <div class="col-md-2">
            <img src="{!!asset('homepage/img/clients/client-6.png')!!}" alt="">
          </div>

        </div>
      </div>
    </section><!-- #more-features -->

    <!--==========================
     
   
      Frequently Asked Questions Section
    ============================-->
   

    <!--==========================
      Our Team Section
    ============================-->
    

    <!--==========================
      Gallery Section
    ============================-->
    <section id="gallery">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">HÌNH ẢNH</h3>
          <span class="section-divider"></span>
          <p class="section-description">Một vài hình ảnh của phần mềm</p>
        </div>

        <div class="row no-gutters">

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-1.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-1.jpg')!!}" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-2.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-2.jpg')!!}" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-3.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-3.jpg')!!}" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-4.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-4.jpg')!!}" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-5.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-5.jpg')!!}" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{!!asset('homepage/img/gallery/gallery-6.jpg')!!}" class="gallery-popup">
                <img src="{!!asset('homepage/img/gallery/gallery-6.jpg')!!}" alt="">
              </a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #gallery -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3>SAVING MONEY </h3>
              <p>Một trong những thương hiệu phần mềm đem lại trải nghiệm tốt nhất cho người dùng trên nền tảng website.Phần mêm đảm bảo cho khách hàng quản lý chi tiêu một cách hợp lý nhất</p>
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>298 Cầu Diễn<br>Bắc Từ Liêm, Hà Nội</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>dhcnhn@gmail.com</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>123456789</p>
              </div>

            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>2017-2018</strong>. All Rights Reserved
          </div>
          
        </div>
        <div class="col-lg-6">
          
        </div>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{!!asset('homepage/lib/jquery/jquery.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/jquery/jquery-migrate.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/bootstrap/js/bootstrap.bundle.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/easing/easing.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/wow/wow.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/superfish/hoverIntent.js')!!}"></script>
  <script src="{!!asset('homepage/lib/superfish/superfish.min.js')!!}"></script>
  <script src="{!!asset('homepage/lib/magnific-popup/magnific-popup.min.js')!!}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="{!!asset('homepage/contactform/contactform.js')!!}"></script>

  <!-- Template Main Javascript File -->
  <script src="{!!asset('homepage/js/main.js')!!}"></script>

</body>
</html>
