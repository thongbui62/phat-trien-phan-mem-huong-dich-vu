<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocumentModel;
use App\User;
use App\CategoryModel;
use phpDocumentor\Reflection\Types\Self_;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Auth;

class AdminController extends Controller
{
    private function convert($time){
        $time = date_parse_from_format('Y-m-d h:i:s',$time);
        $time = mktime(0,0,0,$time['month'],$time['day'],$time['year']);
        $time = date('Y-m-d',$time);
        return $time;
    }
    public function getIndex(){
        $total            = User::where('level',2)->count();
        $now              = date('Y-m-d');
        $allUser          = User::all()->toArray();
        $totalNewRegister = 0;
    	foreach ($allUser as $v){
           $time = $this->convert($v['created_at']);
           if($time == $now && $v['level'] == 2){
               $totalNewRegister++;
           }
        }


    	$arrData = array('totalUser' => $total,'totalNewRegister'=>$totalNewRegister);
    	return view('admin.index',$arrData);
    }
    public function getAllUser(){
        
        $users = User::paginate(10);
        
        if(!empty($_GET)){
            $id = $_GET['id'];
            $check = $_GET['check'];

            if($check == 1){
                $newCheck = 1;
            }else{
                $newCheck = 0;
            }

            echo $newCheck;
           $userA =User::find($id);
           $userA->active = $newCheck;
           $userA->save();
        }
    	 return view('admin.users.user',['list_user'=>$users]);
    }

    public function addUser(){
        return view('admin.users.addUser');
    }

    public function setAddUser(Request $request){
        $name                = $request->name;
        $password            = $request->password;
        $email               = $request->email;
        $content_user        = $request->content_users;
        $level               = $request->level;
        $active              = $request->active;
        $path                = $request->file('images_avatar')->store('images');

        $user                = new User();
        $user->name          = $name;
        $user->password      = bcrypt($password);
        $user->email         = $email;
        $user->content_users = $content_user;
        $user->level         = $level;
        $user->images_avatar = $path;
        $user->active        = ($active == 'on') ? '1' : '0';
        if($user->save()){
            return redirect()->route('admin/index');
        }

    }

    public function addDocument(){
        return view('admin.document.addDocument');
    }

    public function setAddDocument(Request $request){
        $name       = $request->name;
        $content    = $request->contentDocument;
        $file       = $request->file('uploadDocument')->store('document');
        $created_by = Auth::user()->id;
        $document   = new DocumentModel;
        $data       = $document->addDocument($name,$content,$file ,$created_by);
        if($data == true){
            return redirect()->route('admin/index');
        }
    }

    public function getAllDocument(){
        $data = DocumentModel::all()->toArray();
        return view('admin.document.document',['data'=>$data]);
    }

    public function active(){
        echo "Da nhan";
    }

    public function deleteUser($id){
        $userA = User::find($id);
        $userA->delete();
        return redirect()->route('getAllUser');
    }

    public function geteditController($id){
        return view('admin.users.editUser');
    }

    public function getAllCategory(){
        $dataCategory = CategoryModel::all();
        return view('admin.category.category',compact('dataCategory'));
    }

    public function getAddCategory(){
         $dataCategory = CategoryModel::all();
        return view('admin.category.addCategory',compact('dataCategory'));
    }

    public function setAddCategory(Request $request){
        $name = $request->name;
        $category_id =$request->category_id;
        $comment = $request->comment;
        $path = $request->file('imagesCategory')->store('images');
        $category = new CategoryModel;
        $category->name = $name;
        $category->images = $path;
        $category->comment = $comment;
        $category->category_id = $category_id;
        if($category->save()){
            return redirect()->route('getAllCategory');
        }
    }
}
