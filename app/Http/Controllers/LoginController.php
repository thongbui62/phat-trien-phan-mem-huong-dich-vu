<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\loginRequest;

class LoginController extends Controller
{
    public function getRegister(){
    	return view('login.register');
    }

    public function setRegister(Request $rq){
    	
    	$email 		= $rq->email;
    	$username 	= $rq->username;
    	$password 	= $rq->password;

    	$userA = new User();
    	$userA->name 			= 	$username;
    	$userA->password 		=	bcrypt($password);
    	$userA->email 			=	$email;
        $userA->images_avatar   =   'abc.jpg';
        $userA->level           =   2;
        $userA->active          =   1;
    	if($userA->save()){
            return redirect()->route('login');
        }
    }

    public function getLogin(){
         return view('login.login');
    }

    public function setLogin(loginRequest $rq){
        $name = $rq->username;
        $password = $rq->password;

        if(Auth::attempt(['name'=>$name,'password'=>$password])){
            $data = DB::table('users')->where('name','=',$name)->get();
            $level = $data[0]->level;
            $active = $data[0]->active;
            if($active == 1){
                switch ($level) {
                    case '1':
                        session(['login'=>'true']);
                        return redirect()->route('admin/index');
                        break;

                    case '2':
                        return redirect()->route('users/index');
                        break;
                }
            }else{
                return redirect()->back();
            }

           
        }else{
           return redirect()->route('login');
        }
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->forget('login');
        $request->session()->get('login');
        return redirect()->route('login');
    }
    
}
